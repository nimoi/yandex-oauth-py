# LINKS

[PyPi page](https://pypi.python.org/pypi/yandex-oauth-py)

# INSTALLING

```
#!bash

$ sudo pip install yandex-oauth-py
# or use source code package, after extract  
$ python setup.py install 
```

# USAGE

```
#!python

>>> import yandex_oauth
# or from yandex_oauth import OAuthYandex
>>> oa = yandex_oauth.OAuthYandex(
'02c5296b18bc42****************',
'7eb579f49daf4a2***************')
>>> oa.get_code()
# just follow the url to get code for receiving token
'https://oauth.yandex.ru/authorize?response_type=code&client_id=02c5296b18bc42*************'
>>> oa.get_token(2338****)
{u'token_type': u'bearer', 
u'access_token': u'a5f2e953058741******************', 
u'expires_in': 31536000}

```

# LICENSE
The MIT License (MIT)

Copyright (c) 2015 Grudin wa_pis Anton

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.